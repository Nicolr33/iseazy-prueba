<?php

namespace Database\Seeders;


use App\Models\Producto;
use App\Models\Tienda;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class TiendaWithProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(Faker $faker): void
    {
        $tiendas = [];
        for($i = 0; $i < 20; $i++) {
            $tiendas[] = [
                'name' => $faker->text,
                'description' => $faker->text
            ];
        }

        $chunks = array_chunk($tiendas, 20);

        foreach ($chunks as $chunk) {
            Tienda::insert($chunk);
        }



        $tiendas = Tienda::all();
        foreach ($tiendas as $tienda) {


            $products = [];
            for($i = 0; $i < 20; $i++) {
                $newProduct = new Producto();
                $newProduct->nombre = $faker->text;
                $newProduct->stock = $faker->numberBetween(0, 5);
                $products[] = $newProduct;
            }

            $tienda->Productos()->saveMany($products);
        }
    }
}
