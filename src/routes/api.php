<?php

use App\Http\Controllers\ProductoController;
use App\Http\Controllers\TiendaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'tiendas'], function () {
        Route::get('/', [TiendaController::class, 'getListShopWithCount'])->name('getListShopWithCount');
        Route::put('/create', [TiendaController::class, 'createShopAndProducts'])->name('createShopAndProducts');
        Route::get('/{tienda}', [TiendaController::class, 'getShopWithDescription'])->name('getListShopWithDescription');
        Route::patch('/{tienda}', [TiendaController::class, 'editShop']);
        Route::delete('/{tienda}', [TiendaController::class, 'deleteShop']);
    });
    Route::group(['prefix' => 'productos'], function () {
        Route::post('/{producto}/buy', [ProductoController::class, 'buyProduct']);
    });
});
