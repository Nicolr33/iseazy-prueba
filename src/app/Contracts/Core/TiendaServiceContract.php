<?php

namespace App\Contracts\Core;




use App\Http\Requests\CreateShopWithProductsRequest;
use App\Http\Requests\EditShopRequest;
use App\Models\Tienda;
use Illuminate\Support\Collection;

interface TiendaServiceContract
{
    /**
     * Función encargada de listar las tiendas con su respetivo contador de productos
     * @return Collection
     */
    function getListShopWithCount(): Collection;

    /**
     * Función encargada de listar todas las tiendas con solo la descripción y los productos.
     * @param Tienda $tienda
     * @return Tienda
     */
    function getShopWithDescription(Tienda $tienda): Tienda;

    /**
     * Función encargada de crear una tienda de forma obligatoria y de forma opcional productos de la misma.
     * @param CreateShopWithProductsRequest $request
     * @return Tienda
     */
    function createShopAndProducts(CreateShopWithProductsRequest $request): Tienda;

    /**
     * Función encargada de editar los campos de la tienda enviada por parametro.
     * @param Tienda $tienda
     * @param EditShopRequest $request
     * @return Tienda
     */
    function editShop(Tienda $tienda, EditShopRequest $request): Tienda;

    /**
     * Función encargada de eliminar la tienda enviada por parametro
     * @param Tienda $tienda
     * @return Tienda
     */
    function deleteShop(Tienda $tienda): Tienda;
}
