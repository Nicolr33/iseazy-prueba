<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\HttpFoundation\Response;

class EditShopRequest extends BaseFormRequest
{
    /**
     * Validamos si los campos que se vayan a introducir no superen una cantidad de caracteres
     * Esta opcional todos los campos para poder editar libremente (si solo quiero editar uno y no otro)
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['name' => "string", 'description' => "string"])]
    public function rules(): array
    {
        return [
            'name' => 'max:255',
            'description' => 'max:255'
        ];
    }
}
