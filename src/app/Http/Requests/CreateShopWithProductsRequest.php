<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\HttpFoundation\Response;

class CreateShopWithProductsRequest extends BaseFormRequest
{
    /**
     * Validamos si existe en la request el name para la shop y comprobamos tambien
     * si cuando recibamos un valor de productos es un array
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['name' => "string", 'products' => "array"])]
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'products' => 'array'
        ];
    }

}
