<?php

namespace App\Http\Controllers;

use App\Contracts\Core\ProductoServiceContract;
use App\Http\Controllers\Api\ApiController;
use App\Models\Producto;
use App\Services\ProductoService;
use Exception;
use Illuminate\Http\JsonResponse;

class ProductoController extends ApiController
{


    protected ProductoServiceContract $productoService;

    public function __construct()
    {
        $this->productoService = new ProductoService();
    }

    public function buyProduct(Producto $producto) : JsonResponse {
        try {
            $data = $this->productoService->buyProduct($producto);
            return $this->respond($data);
        } catch (Exception $e) {
            $this->statusCode = 500;
            return $this->respondServerError($e->getMessage());
        }
    }
}
