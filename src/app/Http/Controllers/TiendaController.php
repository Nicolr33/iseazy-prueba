<?php

namespace App\Http\Controllers;

use App\Contracts\Core\TiendaServiceContract;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\CreateShopWithProductsRequest;
use App\Http\Requests\EditShopRequest;
use App\Models\Tienda;
use App\Services\TiendaService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class TiendaController extends ApiController
{
    protected TiendaServiceContract $tiendaService;

    public function __construct()
    {
        $this->tiendaService = new TiendaService;
    }

    /**
     * Función del controlador que se encarga de recibir la data del service, en este caso recibe una lista de tiendas
     * con el contador de productos.
     * @return JsonResponse
     */
    public function getListShopWithCount() : JsonResponse
    {
        try {
            $data = $this->tiendaService->getListShopWithCount();
            return $this->respond($data);
        } catch (Exception $e) {
            $this->setStatusCode(500);
            return $this->respondServerError($e->getMessage());
        }
    }

    /**
     * Función del controlador que se encarga de recibir la data del service, en este caso recibe el contenido de la
     * tienda recibida por parametro y muestra la descripcion
     * @param Tienda $tienda
     * @return JsonResponse
     */
    public function getShopWithDescription(Tienda $tienda) : JsonResponse
    {
        try {
            $data = $this->tiendaService->getShopWithDescription($tienda);
            return $this->respond($data);
        } catch (Exception $e) {
            $this->setStatusCode(500);
            return $this->respondServerError($e->getMessage());
        }
    }

    /**
     * Función que recibe un FormRequest para validar si estan los campos en el body y despues crear la tienda en base
     * a esos datos recibidos.
     * @param CreateShopWithProductsRequest $request
     * @return JsonResponse
     */
    public function createShopAndProducts(CreateShopWithProductsRequest $request) : JsonResponse {
        try {
            $data = $this->tiendaService->createShopAndProducts($request);
            return $this->respondCreated($data);
        } catch (Exception $e) {
            $this->setStatusCode(500);
            return $this->respondServerError($e->getMessage());
        }
    }

    /**
     * Función para editar la tienda mediante su "query param" y el body.
     * @param Tienda $tienda
     * @param EditShopRequest $request
     * @return JsonResponse
     */
    public function editShop(Tienda $tienda, EditShopRequest $request) : JsonResponse {
        try {
            $data = $this->tiendaService->editShop($tienda, $request);
            return $this->respond($data);
        } catch (Exception $e) {
            $this->setStatusCode(500);
            return $this->respondServerError($e->getMessage());
        }
    }

    /**
     * Función para eliminar la tienda enviada por el "query param"
     * @param Tienda $tienda
     * @return JsonResponse
     */
    public function deleteShop(Tienda $tienda) : JsonResponse {
        try {
            $data = $this->tiendaService->deleteShop($tienda);
            return $this->respond($data);
        } catch (Exception $e) {
            $this->setStatusCode(500);
            return $this->respondServerError($e->getMessage());
        }
    }
}
