<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tienda extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'description'];

    protected $hidden = ['pivot'];

    // Comentada ya que solo se pide en un endpoint, si queremos el contador de productos en todos los endpoints
    // podemos llamar a esto para que nos lo serialice automaticamente.
    // protected $withCount = ['productos'];

    public function Productos(): BelongsToMany {
        return $this->belongsToMany(Producto::class, 'producto_tiendas');
    }
}
