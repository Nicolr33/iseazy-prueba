<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $hidden = ['pivot'];

    public $timestamps = false;

    public function Tiendas(): BelongsToMany
    {
        return $this->belongsToMany(Tienda::class, 'producto_tiendas');
    }
}
