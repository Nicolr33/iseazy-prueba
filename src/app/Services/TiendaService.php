<?php

namespace App\Services;

use App\Contracts\Core\TiendaServiceContract;
use App\Http\Requests\CreateShopWithProductsRequest;
use App\Http\Requests\EditShopRequest;
use App\Models\Producto;
use App\Models\Tienda;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class TiendaService implements TiendaServiceContract
{
    /**
     * Función encargada de listar las tiendas con su respetivo contador de productos
     * @return Collection
     */
    public function getListShopWithCount(): Collection {
        return Cache::remember("list_shop_withCount", 60 * 5, function () {
            return Tienda::withCount('Productos')->get();
        });
    }

    /**
     * Función encargada de listar todas las tiendas con solo la descripción y los productos.
     * @param Tienda $tienda
     * @return Tienda
     */
    public function getShopWithDescription(Tienda $tienda): Tienda {
        // En el modelo Tienda, hay un comentario explicando otra forma de hacer el withCount pero he decidido usar este
        // ya que se usa de forma individual para ciertos endpoints
        return Cache::remember("shop_" . $tienda->id . "_with_description", 60 * 5, function () use ($tienda) {
            return $tienda->select(['id', 'description'])->where('id', $tienda->id)->withCount('Productos')->with('Productos')->first();
        });
    }

    /**
     * Función encargada de crear una tienda de forma obligatoria y de forma opcional productos de la misma.
     * @param CreateShopWithProductsRequest $request
     * @return Tienda
     */
    public function createShopAndProducts(CreateShopWithProductsRequest $request): Tienda
    {
        $tienda = new Tienda();
        $tienda->name = $request->get('name');
        $tienda->save();

        if($request->has('products')) {
            $products = [];
            foreach($request->products as $product) {
                $newProduct = new Producto();
                $newProduct->nombre = $product;
                $products[] = $newProduct;
            }
            $tienda->Productos()->saveMany($products);
        }

        return $tienda->load('Productos');
    }

    /**
     * Función encargada de editar los campos de la tienda enviada por parametro.
     * @param Tienda $tienda
     * @param EditShopRequest $request
     * @return Tienda
     */
    public function editShop(Tienda $tienda, EditShopRequest $request): Tienda
    {
        $tienda->fill($request->all());
        $tienda->save();

        return $tienda;
    }

    /**
     * Función encargada de eliminar la tienda enviada por parametro
     * @param Tienda $tienda
     * @return Tienda
     */
    public function deleteShop(Tienda $tienda): Tienda
    {
        $tienda->delete();

        return $tienda;
    }
}
