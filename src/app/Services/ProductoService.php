<?php

namespace App\Services;

use App\Contracts\Core\ProductoServiceContract;
use App\Models\Producto;

class ProductoService implements ProductoServiceContract
{

    /**
     * Constante para empezar a alertar de que no hay casi stock
     * @var int  *
     */
    const STOCK_ALERT_MINIMUM = 5;


    /**
     * Función que se encarga de comprobar y comprar el articulo (y rebajar el stock).
     * Dependiendo del stock sale un mensaje u otro.
     * @param Producto $producto
     * @return string[]
     */
    public function buyProduct(Producto $producto): array {
        $tienda = $producto->Tiendas()->first();
        if($producto->stock === 0) {
            return ['message' => 'Ya no hay stock de este producto en la tienda: ' . $tienda->name];
        }

        $producto->stock -= 1;
        $producto->save();

        if($producto->stock <= self::STOCK_ALERT_MINIMUM) {
            return ['message' => 'Se ha comprado el articulo. Solo hay ' . $producto->stock . " unidades"];
        }

        return ['message' => 'Se ha comprado el articulo.'];
    }
}

