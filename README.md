# Prueba tecnica isEazy Laravel

Tiempo estimado que se ha tardado en hacer todo este proyecto: 4h-4.5h.

## Caracteristicas

- Migraciones para crear tablas y tambien añadir nuevos campos a esas tablas adicionales.
- Proyecto dockerizado (Docker) (Nginx, Mysql, Laravel, PHP, Redis, Composer).
- API RESTful con todas las cosas pedidas en la prueba.
- Un test feature para comprobar si funciona una request (php artisan test)
- Validación datos de entrada, Cache, PHPDoc, etc...
- Seeder para hacer un mock y poder testear

## Instalación

Tenemos que tener instalado en el equipo que usemos Docker.

Clonamos el proyecto, iniciamos el entorno docker y cargamos las migraciones

```sh
git clone https://gitlab.com/Nicolr33/iseazy-prueba
cd iseazy-prueba
docker-compose up site --build -d
docker-compose run --rm composer install
cd src
mv .env.example .env
docker exec -it iseazy-php php optimize:clear
docker exec -it iseazy-php php artisan migrate:fresh --seed
```

## Endpoints

| Method               | URL                                        |
|----------------------|--------------------------------------------|
| GET (Get shops)      | http://localhost/api/v1/tiendas/           |
| PUT (Create shop)    | http://localhost/api/v1/tiendas/create     |
| GET (Get shop)       | http://localhost/api/v1/tiendas/{id}       |
| PATCH (Edit shop)    | http://localhost/api/v1/tiendas/{id}       |
| DELETE (Delete shop) | http://localhost/api/v1/tiendas/{id}       |
| POST (Buy Product)   | http://localhost/api/v1/productos/{id}/buy |


#### PUT (Create shop) Body:

```js
{
    "name": "Test",
    "products": [
        "asd", "test2", "test3", "test4"
    ]
}
```

#### PATCH (Edit shop) Body:

```js
{
    "name": "Test",
    "description": "Tedt23"
}
```